json.extract! portfolio, :id, :name, :description, :picture, :created_at, :updated_at
json.url portfolio_url(portfolio, format: :json)
